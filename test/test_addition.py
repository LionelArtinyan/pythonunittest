from src.calculator import add
import pytest

def test_add():
    res = add(3,7)
    assert res == 10

def test_add_string():
    with pytest.raises(TypeError):
        add('7',3)