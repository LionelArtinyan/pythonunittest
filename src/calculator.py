def add(a,b):
    checkinputs(a,b)
    return a+b

def checkinputs(a,b):
    if not isinstance(a,(int,float)) or not isinstance(b,(int,float)):
        raise TypeError ("inputs must be int or float")

a = 1
b = 2
print(add(a,b))